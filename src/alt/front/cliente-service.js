class ClienteService {

    constructor() {
        this.microservice = 'http://localhost:8080';
        this.endpoint='/api/v1/clientes';

        this.token = '';
        document.addEventListener('ce-alt-sso-token', event => {
            this.token = event.detail;
        }, false);
    }

    async list(codigo, nome, endereco, telefone, dataNasc, cursor) {
        codigo = parseInt(codigo);

        let queryString = '';
        if (cursor) {
            queryString = queryString + 'cursor=' + cursor;
        }
        if (dataNasc) {
            if (queryString) {
                queryString = queryString + '&';
            }
            queryString = queryString + 'dataNasc=' + dataNasc;
        }
        if (telefone) {
            if (queryString) {
                queryString = queryString + '&';
            }
            queryString = queryString + 'telefone=' + telefone;
        }
        if (endereco) {
            if (queryString) {
                queryString = queryString + '&';
            }
            queryString = queryString + 'endereco=' + endereco;
        }
        if (nome) {
            if (queryString) {
                queryString = queryString + '&';
            }
            queryString = queryString + 'nome=' + nome;
        }
        if (codigo) {
            if (queryString) {
                queryString = queryString + '&';
            }
            queryString = queryString + 'codigo=' + codigo;
        }

        queryString = encodeURI(queryString);

        const init = {
            headers: {
                Authorization: 'Bearer ' + this.token
            }
        };
        const response = await fetch(this.microservice + this.endpoint + queryString, init);
        if (response.ok) {
            return response.json();
        }
        if (response.status === 401) {
            window.location.reload(true);
        }
        throw new Error('HTTP ' + response.status);
    }

    async put(item) {
        let url = this.microservice + this.endpoint;
        let method = 'POST';

        let dto = {
            id: item.id,
            codigo: item.codigo,
            nome: item.nome,
            telefone: item.telefone,
            dataNasc: item.dataNasc,
            endereco: item.endereco,
        
        };

        if (dto.id) {
            url = url + '/' + dto.id;
            method = 'PATCH';
        }

        const init = {
            headers: {
                Authorization: 'Bearer ' + this.token,
                'Content-Type': 'application/json'
            },
            method: method,
            body: JSON.stringify(dto)
        };
        const response = await fetch(url, init);
        if (response.ok) {
            return response.json();
        }
        if (response.status === 401) {
            window.location.reload(true);
        }
        throw new Error('HTTP ' + response.status);
    }

    async delete(item) {
        const init = {
            headers: {
                Authorization: 'Bearer ' + this.token,
                'Content-Type': 'application/json'
            },
            method: 'DELETE'
        };
        const response = await fetch(this.microservice + this.endpoint + '/' + item.id, init);
        if (response.ok) {
            return response.text();
        }
        if (response.status === 401) {
            window.location.reload(true);
        }
        throw new Error('HTTP ' + response.status);
    }

}
export const clienteService = new ClienteService();

